package tweetacrostics

import opennlp.tools.tokenize.WhitespaceTokenizer
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}
import org.apache.spark.sql.functions.{lit, udf, length, upper, concat}
import scala.io.Source

object TweetAcrostics extends Serializable {

  private def readDictionary: Set[String] = {
    val bufferedSource = Source.fromFile("data/words-lowercase.txt")
    val dictionary = bufferedSource.getLines.toSet
    bufferedSource.close
    dictionary
  }

  // NOTE: the tokenizer must be instantiated within method
  // because the class cannot be serialized
  private def whitespaceTokenize(s: String): Seq[String] =
    WhitespaceTokenizer.INSTANCE.tokenize(s)

  private def buildInitialism(unstructuredText: String): String =
    (for {
      wordToken <- whitespaceTokenize(unstructuredText)
      initial   <- wordToken.headOption
    } yield initial).mkString

  private def isInDictionary(dictionary: Set[String])(string: String): Boolean =
    dictionary.contains(string.toLowerCase)

  def process(dictionary: Set[String],
              unstructuredTweets: Dataset[String])
             (implicit spark: SparkSession): DataFrame = {

    import spark.implicits._

    val dictionaryBroadcast = spark.sparkContext.broadcast(dictionary)

    val buildInitialismUdf = udf(buildInitialism _)
    val isInDictionaryUdf = udf(isInDictionary(dictionaryBroadcast.value) _)

    unstructuredTweets
      .withColumnRenamed("value", "RAW_TWEET")
      .withColumn("INITIALISM", buildInitialismUdf('RAW_TWEET))
      .filter(isInDictionaryUdf('INITIALISM))
      .filter(length('INITIALISM) >= 4) // TODO: consider moving into config
      .select(concat(upper('INITIALISM), lit(" - "), 'RAW_TWEET))

  }

  def main(args: Array[String]) = {

    implicit val spark = SparkSession
      .builder
      .appName("Tweet Acrostics")
      .getOrCreate

    val dictionary = readDictionary

    val rawTweets = spark.read.textFile("data/tweets.txt")

    val acrostics = process(dictionary, rawTweets).collect

    acrostics.map(_(0)).foreach(println)

    println(acrostics.length)

    spark.stop()

  }

}
