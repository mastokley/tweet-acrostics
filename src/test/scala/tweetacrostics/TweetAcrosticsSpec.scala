package tweetacrostics

import org.scalatest._
import org.apache.spark.sql.{Dataset, SparkSession}
import java.util.Arrays

class TweetAcrosticsSpec extends FlatSpec with Matchers {

  import TweetAcrosticsSpec._

  implicit val spark = buildSparkSession
  import spark.implicits._

  "the process function" should "correctly find acrostics" in {

    val received = TweetAcrostics.process(dictionary, unstructuredTweets.toDS)
      .collect
      .map(_(0))

    received.toSet shouldBe expected

  }

  it should "not return any acrostics shorter than 4 characters" in {

    val received = TweetAcrostics.process(dictionary, unstructuredTweets.toDS)
      .collect
      .map(_(0))
      .map(_.toString)

    val allShorterThanFour = received
      .map(_.split(" - "))
      .map(_.head)
      .foldLeft[Boolean](true){ case (acc, cur) => acc && (cur.length >= 4) }

    assert(allShorterThanFour)

  }

}

object TweetAcrosticsSpec {

  def buildSparkSession: SparkSession =
    SparkSession
      .builder()
      .appName("testing")
      .master("local")
      .config("spark.driver.allowMultipleContexts", "false")
      .getOrCreate()

  val dictionary = Set("match", "hand", "coop", "bark", "mom")

  val unstructuredTweets = Seq(
    "My attitude towards college: https://t.co/8VaStW8SSV",
    "have a nice day!",
    "Cadê o onibus porra?!",
    "McCandless: Okay, Mike.",
    "Bir anda rahatladığın kitaplar...",
    "me: it's a surprise",
    "RT @bnishti: צריכה גבר שלא יוותר עליי גם כשאני מוותרת לעצמי.",
    "وابو زينب اليومم في ليالينا 👏💃💃❤ #ZDRSEDK4",
    "ボカロ・UTAU・銀魂・APH・ペルソナ・ハイキュー!!やその他落書きイラストも毎週更新してます",
    "RT @W8: لا إله إلا أنت سبحانك إني كنت من الظالمين #الله_أكبر",
    "- В лотерею поехал играть, и, видимо, выиграл, потому что это было 6 лет назад.",
    "RT @selenagomez: Thankful for these kind hearted fools. 💜🙏☺️#jumeirah #love http://t.co/2mTL92MOzP")

  val expected = Set(
    "MATCH - My attitude towards college: https://t.co/8VaStW8SSV",
    "HAND - have a nice day!",
    "COOP - Cadê o onibus porra?!",
    "BARK - Bir anda rahatladığın kitaplar...")

}
