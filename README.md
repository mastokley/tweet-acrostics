# Project Overview

This application prints out tweets that can be made into acrostics of at least 4 characters. See `data/` for the input files; there's one for the raw tweets and one for the dictionary of permitted words. See `Tweet Acrostics.pdf` for more context.

I'm making use of two third party libraries. The first is Apache OpenNLP for what's called "[tokenization](https://en.wikipedia.org/wiki/Lexical_analysis#Tokenization)": breaking up a sentence into words. In my opinion, even for a straightforward approach to tokenization like whitespace tokenization there's no need to reinvent the wheel when industrial strength libraries exist. (For example, [here's how](https://github.com/apache/opennlp/blob/master/opennlp-tools/src/main/java/opennlp/tools/util/StringUtil.java) OpenNLP determines whether a character is whitespace.) I considered both [Apache OpenNLP](https://opennlp.apache.org/) and [Stanford's CoreNLP](https://stanfordnlp.github.io/CoreNLP/); I couldn't determine that one was better than the other for the specific use case of tokenization, so I picked Apache.

The other library - framework, really - I'm using here is Apache Spark. Full disclosure, this may not be the best library for this task, given how small the data set is. Locally, with 4 cores, it takes something like 20 seconds to process `data/tweets.txt`. Running without Spark, with vanilla Scala, it takes about 4 seconds. If you run the job from within `spark-shell`, you're down to 4 seconds again, not including shell load time. For a data set of this size, the spark startup overhead dwarfs the actual job.

That said, I chose Spark for two reasons. One, knowing very little about Spark, I saw an opportunity to get hands on experience; and two, with Spark, you could scale up your data processing in a way you can't with vanilla Scala.

# Tools Used

I used emacs with [metals](https://scalameta.org/metals/).

# Installation

We'll need spark installed. Download [here](https://www.apache.org/dyn/closer.lua/spark/spark-2.4.5/spark-2.4.5-bin-hadoop2.7.tgz) and extract. You'll make use of the `bin/spark-submit` script later, to run.

We'll also need to manually download and place one of our dependencies. Download [here](https://www.apache.org/dyn/closer.cgi/opennlp/opennlp-1.9.2/apache-opennlp-1.9.2-bin.tar.gz) and place `lib/opennlp-tools-1.9.2.jar` into `lib/`.

In addition, we need two data files downloaded and placed into `data/`. See `Tweet Acrostics.pdf`.

# Building

First, compile, test, and build into a single jar with all dependencies:

    $ sbt assembly

(Note, we're using the sbt plugin `assembly` because sbt's out of the box `package` command excludes jars from `lib/`.)

# Running locally

## Using `spark-submit`

    $ ${SPARK_HOME}/bin/spark-submit \
      --class tweetacrostics.TweetAcrostics \
      --master local \
      ./target/scala-2.12/tweet-acrostics-assembly-0.1.0-SNAPSHOT.jar

## Using `spark-shell`

1. launch `spark-shell`, being sure to include additional dependencies

        $ ${SPARK_HOME}/bin/spark-shell --jars lib/opennlp-tools-1.9.2.jar

2. load the main file and execute main method

        scala> :load src/main/scala/tweetacrostics/TweetAcrostics.scala
        scala> TweetAcrostics.main(Array.empty)

# Testing

This project uses ScalaTest.

    $ sbt test
