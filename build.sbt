import Dependencies._

ThisBuild / scalaVersion     := "2.12.10"
ThisBuild / version          := "0.1.0-SNAPSHOT"
ThisBuild / organization     := "com.example"
ThisBuild / organizationName := "example"

val sparkVersion = "2.4.5"
val opennplVersion = "1.9.2"

lazy val root = (project in file("."))
  .settings(
    name := "tweet-acrostics",
    libraryDependencies += scalaTest % Test,
    libraryDependencies ++= Seq(
      // spark core
      "org.apache.spark" %% "spark-core" % sparkVersion,
      "org.apache.spark" %% "spark-sql" % sparkVersion
    ),
    libraryDependencies += "org.apache.opennlp" % "opennlp-tools" % opennplVersion,
    assemblyMergeStrategy in assembly := {
      case PathList("META-INF", xs @ _*) => MergeStrategy.discard
      case x => MergeStrategy.first
    }
  )

// See https://www.scala-sbt.org/1.x/docs/Using-Sonatype.html for instructions on how to publish to Sonatype.
